"""Like sleep, but keeps the CPU active"""
import sys
from time import time

delta = float(sys.argv[1])

start = time()
while time() < start + delta:
    pass
