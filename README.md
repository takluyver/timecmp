**Timecmp** compares execution time of two similar programs.

## Installation

```
cargo install timecmp
```

`cargo` is Rust's package manager - see https://www.rust-lang.org/

## Usage

```shell
# Compare two different commands
timecmp "sleep 2" -B "python3 sleep_busy.py 2"

# Compare two variants of one script, which checks whether the TIMECMP_AB
# environment variable is set to "A" or "B".
timecmp "./my-script.sh"
```

It will run the two options in alternation, showing a comparison like this:

```
$ timecmp "sleep 2" -B "python3 sleep_busy.py 2"
Running A: ["sleep", "2"]
Running B: ["python3", "sleep_busy.py", "2"]
Running A: ["sleep", "2"]
Running B: ["python3", "sleep_busy.py", "2"]
Running A: ["sleep", "2"]
Running B: ["python3", "sleep_busy.py", "2"]

Elapsed time:
A:     2.002s ███████████████████████████████████████ 
B:     2.051s ████████████████████████████████████████ 
A:     2.003s ███████████████████████████████████████ 
B:     2.040s ███████████████████████████████████████▊
A:     2.001s ███████████████████████████████████████ 
B:     2.047s ███████████████████████████████████████▉

CPU time:
A:    1.431ms  
B:     2.037s ███████████████████████████████████████▉
A:    2.544ms  
B:     2.032s ███████████████████████████████████████▊
A:  749.000µs  
B:     2.042s ████████████████████████████████████████ 
```
