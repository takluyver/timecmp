extern crate clap;
extern crate floating_duration;
extern crate libc;
extern crate shlex;

use clap::{App, Arg};
use floating_duration::{TimeFormat, TimeAsFloat};
use libc::{c_int, pid_t, rusage};
use std::io;
use std::process::{Command, ExitStatus};
use std::os::unix::process::ExitStatusExt;
use std::time::{Instant, Duration};


fn wait4(pid: i32) -> io::Result<(ExitStatus, rusage)> {
    let mut status = 0 as c_int;
    let mut ru: rusage = unsafe { std::mem::zeroed() };
    cvt_r(|| unsafe { libc::wait4(pid, &mut status, 0, &mut ru) })?;
    Ok((ExitStatus::from_raw(status), ru))
}

// Based on cvt_r in the rust standard library, simplified for this specific use case.
// Copyright 2014 The Rust Project Developers.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.
fn cvt_r<F>(mut f: F) -> io::Result<pid_t>
    where F: FnMut() -> pid_t
{
    loop {
        match f() {
            -1 => {
                let e = io::Error::last_os_error();
                if e.kind() != io::ErrorKind::Interrupted {
                    return Err(e);
                }
            }
            other => return Ok(other),
        }
    }
}


// Convert C timeval struct (two integers) to a Rust Duration struct
fn timeval_to_duration(tv: libc::timeval) -> Duration {
    Duration::new(tv.tv_sec as u64, (tv.tv_usec as u32) * 1000)
}

fn bar(n: u32) -> String {
    let opts = [' ', '▏', '▎', '▍', '▌', '▋', '▊', '▉', '█'];
    let mut res = "█".repeat(n as usize / 8);
    res.push(opts[n as usize % 8]);
    return res
}

fn print_time_bars(times: Vec<Duration>, ab_records: &[&str]) {
    let dur_max = times.iter().map(|d| d.as_fractional_secs()).fold(std::f64::NAN, f64::max);
    for (d, ab) in times.iter().zip(ab_records) {
        let ds = format!("{}", TimeFormat(d));
        let dur_normed = ((d.as_fractional_secs() / dur_max) * 320.) as u32;
        println!("{}: {:>10} {}", ab, ds, bar(dur_normed));
    }
}


fn run_cmd_time(args: &[String], ab: &str) -> io::Result<(ExitStatus, Duration, rusage)> {
    let start_time = Instant::now();
    let (cmd, extra_args) = args.split_first().unwrap();
    let child = Command::new(cmd)
                            .args(extra_args)
                            .env("TIMECMP_AB", ab)
                            .spawn()?;
    
    let (ecode, ru) = wait4(child.id() as i32)?;
    let duration = start_time.elapsed();
    Ok((ecode, duration, ru))
}

fn time_cmp(args_a: &[String], args_b: &[String]) {
    let mut cpu_times = Vec::new();
    let mut wall_times = Vec::new();
    let mut ab_records = Vec::new();
    for i in 0..6 {
        let (args, letter) = match i % 2 {
            0 => (args_a, "A"),
            _ => (args_b, "B")
        };
        println!("Running {}: {:?}", letter, args);
        let (exit, duration, ru) = run_cmd_time(args, letter).unwrap();
        if !exit.success() {
            if let Some(sig) = exit.signal() {
                println!("Command {:?} exited with signal {}", args, sig);
            } else {
                println!("Command {:?} exited with status {}", args, exit.code().unwrap());
            }
            std::process::exit(1);
        }

        let cpu_time = timeval_to_duration(ru.ru_utime) + timeval_to_duration(ru.ru_stime);
        cpu_times.push(cpu_time);
        wall_times.push(duration);
        ab_records.push(letter);
    }
    println!("\nElapsed time:");
    print_time_bars(wall_times, ab_records.as_slice());
    println!("\nCPU time:");
    print_time_bars(cpu_times, ab_records.as_slice());
}

fn main() {
    let version = env!("CARGO_PKG_VERSION");
    let matches = App::new("timecmp")
                    .version(version)
                    .author("Thomas Kluyver")
                    .about("Compare execution time & resource usage.")
                    .arg(Arg::with_name("cmd_a")
                         .value_name("CMD-A")
                         .required(true)
                    )
                    .arg(Arg::with_name("cmd_b")
                         .short("B")
                         .value_name("CMD-B")
                    )
    .get_matches();

    let cmd_a = matches.value_of("cmd_a").unwrap();
    let args_a = shlex::split(cmd_a).unwrap();
    let args_b = shlex::split(matches.value_of("cmd_b").unwrap_or(cmd_a)).unwrap();
    time_cmp(&args_a, &args_b);
}
